﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixExaminator
{
    class Matrix
    {
        int rows, cols;
        double[,] mat;
        static Random rand = new Random();


        public Matrix(int _rows, int _cols)
        {
            rows = _rows;
            cols = _cols;

            mat = new double[rows, cols];
        }
        public Matrix(double[,] pole)
        {
            rows = pole.GetLength(0);
            cols = pole.GetLength(1);

            mat = pole;
        }
        public void fillRandom()
        {

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    mat[i, j] = rand.Next() % 30;
                }
            }
        }
        public override int GetHashCode() // vyzadovalu pretizeni z duvodu pretizenych operatoru == a !
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)// vyzadovalu pretizeni z duvodu pretizenych operatoru == a !
        {
            return base.Equals(obj);
        }
        public override string ToString() //vypsani do labelu v tomto formatu
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    str.AppendFormat("{0,3}", mat[i, j]);
                }
                str.AppendLine();
            }
            return str.ToString();
        }
        public static Matrix operator +(Matrix a, Matrix b)
        {
            Matrix tempMat = new Matrix(a.rows, a.cols);

            for (int i = 0; i < a.rows; i++)
            {
                for (int j = 0; j < a.cols; j++)
                {
                    tempMat.mat[i, j] = a.mat[i, j] + b.mat[i, j];
                }
            }

            return tempMat;
        }
        public static bool operator ==(Matrix a, Matrix b)
        {
            if ((object)a == null && (object)b == null)
                return true;
            if (b == null)
                return false;
            for (int i = 0; i < a.rows; i++)
            {
                for (int j = 0; j < a.cols; j++)
                {
                    if (!(a.mat[i, j] == b.mat[i, j]))
                    {
                    return false;
                    }
                }
            }
            return true;
        }
        public static bool operator !=(Matrix a, Matrix b)
        {

            for (int i = 0; i < a.rows; i++)
            {
                for (int j = 0; j < a.cols; j++)
                {
                    if (a.mat[i, j] == b.mat[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        public static Matrix operator *(Matrix a, Matrix b)
        {
            Matrix tempMat = new Matrix(a.rows, b.cols);
            double mezivypocet = 0;
            int i = 0, j = 0;
            for (i = 0; i < a.rows; i++)
            {
                for (j = 0; j < b.cols; j++)
                {
                    mezivypocet = 0;
                    for (int k = 0; k < a.rows; k++)
                    {
                        mezivypocet += a.mat[i, k] * b.mat[k, j];
                    }
                    tempMat.mat[i, j] = mezivypocet;
                }
            }

            return tempMat;
        }
        public double det()
        {

            double d = 0;
            switch (rows)
            {
                case 1:
                    d = mat[0, 0];
                    break;
                case 2:
                    d = ((mat[0, 0] * mat[1, 1]) - (mat[1, 0] * mat[0, 1]));
                    break;
                case 3:
                    d += mat[0, 0] * mat[1, 1] * mat[2, 2];
                    d += mat[1, 0] * mat[2, 1] * mat[0, 2];
                    d += mat[2, 0] * mat[0, 1] * mat[1, 2];

                    d -= mat[2, 0] * mat[1, 1] * mat[0, 2];
                    d -= mat[0, 0] * mat[2, 1] * mat[1, 2];
                    d -= mat[1, 0] * mat[0, 1] * mat[2, 2];
                    break;
                default:
                    throw new Exception("cannot solve determinant of this dimension");
            }

            return d;
        }
    }
}


