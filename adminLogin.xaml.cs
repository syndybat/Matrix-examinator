﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatrixExaminator
{
    /// <summary>
    /// Interaction logic for adminForm.xaml
    /// </summary>
    public partial class adminForm : Window
    {
        public adminForm()
        {
            InitializeComponent();
        }
        private void btnZpetClick(object sender, RoutedEventArgs e)
        {
            this.Close();
            Application.Current.MainWindow.Show();
        }

        private void btnPotvrdClick(object sender, RoutedEventArgs e)
        {
            lblError.Content = "";
            if (txbAdmin.Password == "admin")
            {
                settingWindow setWin = new settingWindow();
                this.Close();
                setWin.ShowDialog();
            }
            else
            {
                txbAdmin.Password = "";
                lblError.Content = "spatne heslo";
            }
        }
    }
}
