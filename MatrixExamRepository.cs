﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixExaminator
{
    class MatrixExamRepository
    {
        List<ExampleDet> exam = new List<ExampleDet>(); // list obsahujici priklady
        List<setting> set; // list obsahujici konfiguraci z settings(dimenze, operace, pocet prikladu)
        public enum operation {plus,multiply,determinant}; // mozne operace

        public MatrixExamRepository(List<setting> _set) {
            set = _set;
        }

        public List<ExampleDet> generateExam() {
            exam.Clear();
            foreach(setting value in set)
            {
                if(value.operation == (int)operation.plus)
                {
                    for(int i = 0; i < value.count; i++)
                    {
                        getSumMatrixs(value.dimension);
                    }
                }
                if (value.operation == (int)operation.multiply)
                {
                    for (int i = 0; i < value.count; i++)
                    {
                        getMultipleMatrixs(value.dimension);
                    }
                }
                if (value.operation == (int)operation.determinant)
                {
                    for (int i = 0; i < value.count; i++)
                    {
                        getDetMatrix(value.dimension);
                    }
                }

            }
            
            return exam;
        }
        public testResult verify()
        {
            testResult tr = new testResult();
            tr.celkemPrikladu = set.Sum(x => x.count);
            tr.body = 0;
            tr.dobrePrikladu = 0;

            foreach(ExampleDet example in exam) //prochazi cely list obsahujici ExampleDet a Example
            {                                   //verifikace je implementovana ve yminenych tridach
                if(example.verify())
                {
                    tr.dobrePrikladu++;
                }
            }
            tr.body = tr.dobrePrikladu*2;
            return tr;
        }
        private void getSumMatrixs(int _dimension = 3) { //generuje priklad pro scitani
            Matrix temMat1 = new Matrix(_dimension, _dimension);
            Matrix temMat2 = new Matrix(_dimension, _dimension);

            temMat1.fillRandom();
            temMat2.fillRandom();

            Example example = new Example(temMat1, operation.plus, temMat2);
            exam.Add(example);
        }
        private void getMultipleMatrixs(int _dimension = 3)//generuje priklad pro nasobeni
        {
            Matrix temMat1 = new Matrix(_dimension, _dimension);
            Matrix temMat2 = new Matrix(_dimension, _dimension);

            temMat1.fillRandom();
            temMat2.fillRandom();

            Example example = new Example(temMat1, operation.multiply, temMat2);
            exam.Add(example);
        }
        private void getDetMatrix(int _dimension = 3)//generuje priklad pro determinant
        {
            Matrix temMat1 = new Matrix(_dimension, _dimension);

            temMat1.fillRandom();

            ExampleDet example = new ExampleDet(temMat1, operation.determinant);
            exam.Add(example);
        }
    }
}
