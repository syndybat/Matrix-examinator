﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MatrixExaminator;

namespace MatrixExaminator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataClassesDataContext db;
        public MainWindow()
        {
            InitializeComponent();
            db = new DataClassesDataContext();
        }
       
        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
           Window testW = new testLogin(db);
            
            testW.ShowDialog();
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            Window adminForm = new adminForm();
            adminForm.ShowDialog();

            
        }
    }
}
