﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixExaminator
{
    class ExampleDet
    {
        protected Matrix mat;
        protected string result; // vysledek pro docasne ulozeni pri probihajicim testu
        protected MatrixExamRepository.operation operation;

        public MatrixExamRepository.operation Operation { get { return operation; } }
        public string Result { get { return result; } set { result = value; } }
        public Matrix Mat { get { return mat; } }
        public ExampleDet(Matrix _mat, MatrixExamRepository.operation _operation)
        {
            mat = _mat;
            result = "";
            operation = _operation;
        }
        virtual public bool verify() // overi zda vysledek daneho prikladu koresponduej s naparsovanym result
        {
            double value = parse();
            if (value != Double.NaN && value == mat.det())
            {
                return true;
            }
            return false;
        }
        public double parse()
        {
            if (result == "")
                return Double.NaN;
            double temp = 0;
            try
            {
                temp = Double.Parse(result);
            }
            catch (FormatException)
            {
                throw new FormatException();
            }
            return temp;
        }
    }
}
