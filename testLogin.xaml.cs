﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatrixExaminator
{
    /// <summary>
    /// Interaction logic for testLogin.xaml
    /// </summary>
    public partial class testLogin : Window
    {
        DataClassesDataContext db;
        public testLogin(DataClassesDataContext _db)
        {
            InitializeComponent();
            db = _db;

        }

        private void btnSpustTest_Click(object sender, RoutedEventArgs e)
        {
            lblError.Content = "";
            if (db.users.Any(x => x.name == txbUsername.Text))
            {
                user student = new user();
                student.name = txbUsername.Text;
                Window testWindow = new testWindow(db, student);
                
               
                testWindow.Show();
                this.Close();
            }
            else
            {
                lblError.Content = "spatne jmeno: "+ txbUsername.Text;
            }
        }
    
    }
}
