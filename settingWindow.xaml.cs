﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatrixExaminator
{
    /// <summary>
    /// Interaction logic for settingWindow.xaml
    /// </summary>
    public partial class settingWindow : Window
    {
        DataClassesDataContext db = new DataClassesDataContext();
        IEnumerable<setting> settings; //nastaveni z db(settings)
        public settingWindow()
        {
            InitializeComponent();
            settings = db.settings;

            //nastaven comboboxu operaci
            cbxOperace.Items.Add(MatrixExamRepository.operation.plus);
            cbxOperace.Items.Add(MatrixExamRepository.operation.multiply);
            cbxOperace.Items.Add(MatrixExamRepository.operation.determinant);
            cbxOperace.SelectedIndex = 0;
            //nastaveni listboxu
            string str = "";
            foreach (var val in settings)
            {
                str = String.Format("operace: {0}, dimenze: {1},pocet: {2}", (MatrixExamRepository.operation)val.operation, val.dimension, val.count);
                listBox.Items.Add(str);
            }

            repaint();            
        }
        private void repaint()
        {
            //editacni pole vyplni na zaklade vybraneho itemu v listboxu
            int index = listBox.SelectedIndex;
            if(index!=-1)
            {
                txbDimension.Text = settings.ElementAt(index).dimension.ToString();
                txbPocet.Text = settings.ElementAt(index).count.ToString();
                cbxOperace.SelectedIndex = settings.ElementAt(index).operation;
            }
        }
        private void repaintCompletely()
        {
            //navic prekresli listbox pri editaci
            for(int i = listBox.Items.Count-1;i>=0;i--)
            {
                listBox.Items.RemoveAt(i);
            }
            string str = "";
            foreach (var val in settings)
            {
                str = String.Format("operace: {0}, dimenze: {1},pocet: {2}", (MatrixExamRepository.operation)val.operation, val.dimension, val.count);
                listBox.Items.Add(str);
            }
            
                txbDimension.Text = "";
                txbPocet.Text = "";
                cbxOperace.SelectedIndex = 0;
            
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
         //   listBox.Items.Clear();
            repaint();
        }

        private void btnPotvrd_Click(object sender, RoutedEventArgs e)
        {
            int dimenze, pocet;

            if (!Int32.TryParse(txbPocet.Text, out pocet))
            {
                if (pocet < 1)
                {
                    MessageBox.Show("Mušíš zadat počet");
                    return;
                }
                
            }
            else if (pocet < 1)
            {
                MessageBox.Show("Nesmíš zadat 0");
                return;
            }
            

            if (!Int32.TryParse(txbDimension.Text, out dimenze))
            {
                if (pocet < 1)
                {
                    MessageBox.Show("Mušíš zadat počet");
                    return;
                }

            }
            else if (pocet < 1)
            {
                MessageBox.Show("Nesmíš zadat 0");
                return;
            }

            if (dimenze > 3 && (int)MatrixExamRepository.operation.determinant == cbxOperace.SelectedIndex)
                {
                    MessageBox.Show("Neumím vypočítat determinant z matice větší jak 3x3");
                    return;
                }

                setting temp = new setting();
                temp.count = (short)pocet;
                temp.dimension = (short)dimenze;
                temp.operation = (int)cbxOperace.SelectedItem;

               
                if(db.settings.Any(x=>(x.operation == temp.operation && x.dimension==temp.dimension)))//overeni zda kombinace operace dimenze existuje
                {
                var tempset=  db.settings.Where(x => (x.operation == temp.operation && x.dimension == temp.dimension)).First();
                    tempset.count = temp.count;
                }
                else
                {
                    db.settings.InsertOnSubmit(temp);
                }
            db.SubmitChanges();

            repaintCompletely();
       
    }

        private void btnDelete1_Click(object sender, RoutedEventArgs e)
        {
            int index = listBox.SelectedIndex;
            if (index != -1)
            {
                db.settings.DeleteOnSubmit(settings.ElementAt(index));
                db.SubmitChanges();
            }
            else
            {
                MessageBox.Show("vyberte item pro smazani nejprve");
                return;
            }
            repaintCompletely();
        }

        private void btnZpet_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
