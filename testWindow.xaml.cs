﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatrixExaminator
{
    /// <summary>
    /// Interaction logic for testWindow.xaml
    /// </summary>
    public partial class testWindow : Window
    {
        int pocetPrikladu;
        int soucasnyPriklad;
        MatrixExamRepository matExam;
        DataClassesDataContext db;
        List<setting> lSeting;   //soucasne nastaveni testu predavane MatrixExamRepository
        List<ExampleDet> lExam; // list obsahujici priklady vygenerovany z MatrixExamRepository
        user student;

        public testWindow(DataClassesDataContext _db, user _student)
        {
            Application.Current.MainWindow.Hide();
            InitializeComponent();
            student = _student;
            lblName.Content = student.name;

            db = _db;
            lSeting = db.settings.OrderBy(x=>x.operation).ToList();
            pocetPrikladu = lSeting.Sum(x => x.count);
            soucasnyPriklad =0;
            
            matExam = new MatrixExamRepository(lSeting);
            lExam = matExam.generateExam();

            Repaint();
        }
        public void Repaint()
        {
            lblMatrixA.Content = "";
            lblMatrixB.Content = "";
            lblPocet.Content = String.Format("{0}/{1}", soucasnyPriklad+1, pocetPrikladu);
            txbVysledek.Text = lExam.ElementAt(soucasnyPriklad).Result;
            

            ExampleDet exampleDet = lExam.ElementAt(soucasnyPriklad);
            if (exampleDet is Example)
            {
                Example example = exampleDet as Example;
                lblMatrixA.Content = example.Mat;
                lblMatrixB.Content = example.Mat2;
            }
            else
            {
                lblMatrixA.Content = exampleDet.Mat;
            }

            switch (exampleDet.Operation)
            {
                case MatrixExamRepository.operation.plus:
                    lblOperace.Content = "Secti dve matice";
                    return;
                case MatrixExamRepository.operation.multiply:
                    lblOperace.Content = "Vynasob dve matice";
                    return;
                case MatrixExamRepository.operation.determinant:
                    lblOperace.Content = "Vypocti Determinant";
                    return;
            }
          
           
        }

        private void btnZpet_Click(object sender, RoutedEventArgs e)
        {
            
            if (soucasnyPriklad>0)
            {
               lExam.ElementAt(soucasnyPriklad).Result = txbVysledek.Text;
               soucasnyPriklad--;
               Repaint();
            }
            
        }

        private void btnDalsi_Click(object sender, RoutedEventArgs e)
        {
            if (soucasnyPriklad < pocetPrikladu-1)
            {
                lExam.ElementAt(soucasnyPriklad).Result = txbVysledek.Text;
                soucasnyPriklad++;
                Repaint();
            }
            
        }

        private void btnOdevzdat_Click(object sender, RoutedEventArgs e)
        {
            lExam.ElementAt(soucasnyPriklad).Result = txbVysledek.Text;
            if(MessageBox.Show("opravdu chcete odevzdat?", "Potvrzeni", MessageBoxButton.YesNo)==MessageBoxResult.No)
            {
                return;
            }
            testResult result = null;
            try
            {
                result = matExam.verify(); //zachytavani chyby pri parsovani
            }
            catch (Exception exc)
            {
                MessageBox.Show(String.Format( "Došlo k chybě: {0}", exc.Message));
                return;
            }
            

            string str = String.Format("Celkem příkladů: {0}, správně příkladů: {1}, získaný počet bodů: {2}"
                ,result.celkemPrikladu,result.dobrePrikladu,result.body);
            MessageBox.Show(str, "Vysledek", MessageBoxButton.OK);

            result.userName = student.name;
            db.testResults.InsertOnSubmit(result);
            db.SubmitChanges();
            
            this.Close();

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.MainWindow.Show();
        }
    }
}
