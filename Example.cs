﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixExaminator
{
    class Example : ExampleDet
    {
        Matrix mat2;
        public Matrix Mat2 { get { return mat2; } }
        public Example(Matrix _mat, MatrixExamRepository.operation _operation, Matrix _mat2) : base(_mat,_operation)
        {
            mat2 = _mat2;
        }
        public override bool verify()
        {
            Matrix temp = parse();
            if (temp == null)
                return false;
            if(operation == MatrixExamRepository.operation.plus)
            {
                if (temp == (mat + mat2))
                {
                    return true;
                }
            }
            else
            {
                if (temp == (mat * mat2))
                {
                    return true;
                }
            }
            
            return false;
        }
        public new Matrix parse() // new pouzito z duvodu jineho navratoveho typu
        {
            if (result == "")
                return null;
            char[] MyChar = { '[', ']', '{', '}' };      //pokud by pridaval prvky [1 1 1 ; 2 2 2; 5 6 3] nebo {} tak je to smaze a np nehazi chybu potom
            result = result.Trim(MyChar);
            string[] rows,values;
            rows = result.Split(new[]{';'}, StringSplitOptions.RemoveEmptyEntries);

            int count = rows.Count();
            double[,] mat = new double[count, count];

            try { // v pripade zadani pismen nebo matice ve spatnem formatu vyhodi chybu
                for (int i = 0; i < count; i++)
                {
                    values = rows[i].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 0; j < count; j++)
                    {
                        mat[i, j] = Double.Parse(values[j]);
                    }
                }
            }
            catch(Exception)
            {
                throw new Exception("Spatny format");
            }
            
            return new Matrix(mat);
        }
    }
}
